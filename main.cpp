/*
 *    _____________________________
 *   /                             \
 *   |     Bender's Day Out.       |
 *   \_____________________________/
 *
 *   Author: Matthew Healy
 *
 *   Section: A
 *
 *   Version: 1.1.0
 *
 *   Purpose: implements Dijkstra's algorithm to find the
 *            least costly path from the airport to the bar
 *
 */
#include "dijkstra.h"

using namespace std;

int main() {

  string current_loc;
  map<string, int> locations;
  int num_locations;
  int paths_out;
  string endpoint;
  int endpoint_ref;
  int weight;
  int loc_last = 0;
  string city;
  int cities;

  cin >> cities;
  for(int counter = 1; counter <= cities; counter++) {
    cin >> city;
    cout << "#" << counter << " : " << city << ", ";
    cin >> num_locations;
    
    Graph<string> city_graph(num_locations);
    for(int i=0; i<num_locations; i++) {
      cin >> current_loc;
      cin >> paths_out;
      for(int j=0;j<paths_out;j++) {
        cin >> endpoint;
        cin >> weight;
        city_graph.insert(current_loc,endpoint,weight);
      }
    }

    cout << dijkstra(city_graph) << " tokens." << endl;
  }
  return 0;
}

