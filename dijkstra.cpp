/*
 *    _____________________________
 *   /                             \
 *   |     Dijkstra's Algorith     |
 *   \_____________________________/
 *
 *   Author: Matthew Healy
 *
 *   Section: A
 *
 *   Version: 1.1.1
 *
 *   Purpose: implements Dijkstra's algorithm to find the
 *            least costly path from the airport to the bar
 *
 */

#include "graph.h"
#include "dijkstra.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <map>
#include <string>

using namespace std;


int dijkstra(Graph<string> &graph) {
  int start_point;
  int end_point;
  int current,weight;
  priority_queue <pair<int,int>, vector<pair<int,int> >, Less > heap;
  int dist[graph.m_size];

  fill_n(dist,graph.m_size,INT_MAX);
  start_point = graph.node_refs.find("airport")->second;
  end_point = graph.node_refs.find("robotbar")->second;

  dist[start_point] = 0;
  heap.push(pair<int,int>(start_point,0));

  while(!heap.empty()) {
    current = heap.top().first;
    weight = heap.top().second;
    heap.pop(); //remove top element
    for(int i=0;i<graph.m_size;i++) {
      int new_weight = INT_MAX;
      if(graph.adj_matrix[current][i] != 0) {
        int new_weight = weight + graph.adj_matrix[current][i];
      
        if(dist[i] > new_weight) {
          heap.push(pair<int,int>(i,new_weight));
          dist[i] = new_weight;
        }
      }
    }

  }


  return dist[end_point];
}
