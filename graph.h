/*    _______________
 *   /               \
 *   |     Graph     |
 *   \_______________/
 *
 *   Author: Matthew Healy
 *
 *   Section: A
 *
 *   Version: 1.1.0
 *
 *   Purpose: A templated class that defines a graph,
 *            a set of connected nodes, where the connections
 *            between nodes have both directions and weight.
 *
 *   Note: Graph is an acyclic graph, and MUST be passes an argument(size)
 *         when constructing
 */

#ifndef GRAPH_H
#define GRAPH_H

#include <map>

using namespace std;

template <class T>
class Graph {

  int last_noderef;
public:
  int m_size;
  int** adj_matrix;
  map<T, int> node_refs;
  
  // Purpose: Constructor
  // Parameters: Number of nodes in graph
  Graph<T>(int size) : m_size(size), last_noderef(0){
    adj_matrix = new int* [m_size];
    for(int i=0; i < m_size; i++)
      adj_matrix[i] = new int[size];
    for(int i=0;i<m_size;i++)
      for(int j=0;j<m_size;j++)
        adj_matrix[i][j] = 0;
  };

  // Purpose: Deconstructor
  ~Graph<T>();

  // Purpose: Insert path from current_loc to endpoint with weight
  // Parameters: source, destination, weight of path
  // Returns: nothing
  void insert(T current_loc, T endpoint, int weight);
};

#include "graph.hpp"
#endif
