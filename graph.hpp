/*
 *  _____________
 * /             \
 * |    Graph    |
 * \_____________/
 *
 * Author: Matthew Healy
 *
 * Section: A
 *
 * Version: 1.1.0
 *
 * Purpose: Implementation of Graph<T> class
 */

#ifndef GRAPH_HPP
#define GRAPH_HPP

using namespace std;

// Purpose: inserts weighted connection between current and endpoint
//          abstracts adjacency matrix away from user
// Requirements: adj_matrix must already be defined
// Results: weighted connection is loaded
template<typename T>
void Graph<T>::insert(T current_loc, T endpoint, int weight) {
  int int_location, int_endpoint;
  typename map<T,int>::iterator iter;
    
  iter = node_refs.find(current_loc);
  if(iter == node_refs.end()) {
    node_refs.insert(iter, pair<T,int>(current_loc,last_noderef));
    last_noderef++;
  } 

  int_location = node_refs.find(current_loc)->second;

  iter = node_refs.find(endpoint);
  if(iter == node_refs.end()) {
    node_refs.insert(iter, pair<T,int>(endpoint,last_noderef));
    last_noderef++;
  } 

  int_endpoint = node_refs.find(endpoint)->second;

  adj_matrix[int_location][int_endpoint] = weight;

  return;
}


//Purpose: deconstructor
//         deletes adj_matrix correctly
template<typename T>
Graph<T>::~Graph<T>() {
  for(int i=0;i<m_size;i++)
    delete [] adj_matrix[i];
  delete [] adj_matrix;
}

#endif
