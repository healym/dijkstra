/*
 *    _____________________________
 *   /                             \
 *   |     Bender's Day Out.       |
 *   \_____________________________/
 *
 *   Author: Matthew Healy
 *
 *   Section: A
 *
 *   Version: 0.1.1
 *
 *   Purpose: implements Dijkstra's algorithm to find the
 *            least costly path from the airport to the bar
 *
 */

#include "graph.h"
#include <iostream>
#include <tuple>
#include <map>
#include <string>
#include <queue>
#include <algorithm>

// Purpose: find shortest path in graph from airport to bar
// Parameters: graph with string node names
// Returns: length of shortest path
int dijkstra(Graph<string> &graph);
const int INT_MAX = 32767;

// Comparator class for priority_queue
class Less {
public:
  bool operator() (pair<int,int> lhs,pair<int,int> rhs) {
    return lhs.second < rhs.second ? true : false;
  }
};
